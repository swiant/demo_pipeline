"""
Brief demo of uploading to dropbox
"""
import argparse

import dropbox
from dropbox.files import WriteMode


parser = argparse.ArgumentParser(description='Upload Forms to Dropbox')
parser.add_argument('--token', help='Access token (see https://www.dropbox.com/developers/apps)', required=True)

args = parser.parse_args()

dbx = dropbox.Dropbox(args.token)

with open('forms/keeper_form.txt', 'rb') as f:
    dbx.files_upload(f.read(), "/demo_pipeline/keeper_form.txt", mode=WriteMode.overwrite)
